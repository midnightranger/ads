#!/bin/bash

# A minimal Installation for Arch Linux. It is assumed that a vanilla Arch Linux system is available!
# For a vanilla Arch Linux Installation (the Arch-way), check my corresponding repo on gitlab.com.

## SOME VARIABLES

# Declare where are the packages you wish to install as well as which AUR helper to use 
Aurhelper="yay"
progsfile="./progs.csv"
aurfile="./aur.csv"

preinst(){ \
	echo "Updating ARCH cache and perform system-update..." && sudo pacman -Syu --noconfirm > /dev/null 2>&1
}

intel-dpl(){ \
	echo "Installing Intel Video Driver..." && sudo pacman -S --noconfirm xf86-video-intel mesa libva-intel-driver libva-utils > /dev/null 2>&1
	
}

nvidia_lts-dpl(){ \
	echo "Installing NVidia LTS Video Driver..." && sudo pacman -S --noconfirm nvidia-lts nvidia-utils > /dev/null 2>&1

}

nvidia-dpl(){ \
	echo "Installing NVidia Video Driver..." && sudo pacman -S --noconfirm nvidia nvidia-utils > /dev/null 2>&1

}

virtualbox-dpl(){ \
	echo "Installing Virtualbox-guest Video Driver..." && sudo pacman -S --noconfirm virtualbox-guest-utils xf86-video-vmware > /dev/null 2>&1

}

xdeployment(){ \
	echo "Installing X-Server..." && sudo pacman -S --noconfirm xorg xorg-xinit > /dev/null 2>&1
}

aurhelperinstall(){ #Install AUR helper
	[ -f "/usr/bin/$1" ] || (
	echo "Installing "$1", an AUR helper..."
	cd /tmp
	rm -rf /tmp/"$1"*
	curl -SO https://aur.archlinux.org/cgit/aur.git/snapshot/"$1".tar.gz > /dev/null 2>&1
	tar -xf "$1".tar.gz > /dev/null 2>&1 && cd "$Aurhelper"
	makepkg --noconfirm -si > /dev/null 2>&1
	) ;}

install(){ \
	echo "Installing from ARCH Repo: $1 ..." && sudo pacman -S --noconfirm "$1" > /dev/null 2>&1
}

aurinstall(){ \
	echo "Installing from AUR: $1 ..." && $Aurhelper -S --noconfirm "$1" > /dev/null 2>&1
}	

installationloop(){ \
	while IFS='' read program; do
		install $program
	done < $progsfile
}

aurloop(){ \
	while IFS='' read program; do
		aurinstall $program
	done < $aurfile
}

dwmxsesentry(){
	echo "Installing lightdm display manager ..." && sudo pacman -S --noconfirm lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings > /dev/null 2>&1
	echo "Make dwm available to lightdm sessions ... " && sudo cp dwm.desktop /usr/share/xsessions/
	echo "Fix permissions for dwm xsession ... " && sudo chmod 644 /usr/share/xsessions/dwm.desktop
	echo "Enabling lightdm service for next rebbot ..." && sudo systemctl enable lightdm
}	

## MAIN SCRIPT

clear
preinst
#intel-dpl
#nvidia_lts-dpl
#nvidia-dpl
virtualbox-dpl
xdeployment
#aurhelperinstall $Aurhelper
installationloop
#aurloop
dwmxsesentry
